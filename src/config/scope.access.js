export default [
    {
        "scope": [
            "registrar"
        ],
        "resource": "/api/TestActions/?$",
        "access": [
            "read",
            "write"
        ]
    },
    {
        "scope": [
            "students"
        ],
        "resource": "/api/TestActions/?$",
        "access": [
            "read",
            "write"
        ]
    }
]